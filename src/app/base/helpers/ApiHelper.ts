import { AxiosRequestConfig } from 'axios';

import { appConfig } from '../../appConfig';

export default class ApiHelper {
  static customizeHeaders(config: AxiosRequestConfig, headers: {}) {
    if (config.method?.toLocaleLowerCase() !== 'get' && !config.url?.includes('logout') && appConfig.customHeaders) {
      headers = Object.assign(headers, appConfig.customHeaders);
    }

    return headers;
  }
}
