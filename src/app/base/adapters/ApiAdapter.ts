import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { appConfig } from '../../appConfig';
import TokenService from '../../modules/token/TokenService';
import ApiHelper from '../helpers/ApiHelper';
import { RootStoreContext, storesContext } from '../RootStore';

export const api = axios.create();

export const SUCCESS_STATUSES = [200, 201, 202];
export const SERVER_ERROR = 500;
export const AUTH_ERROR = [401, 403]
export const NOT_FOUND_ERROR = [404]
export const BADREQUEST = [400]


api.defaults.baseURL = appConfig.apiUrl;

api.interceptors.request.use(
  async (config: AxiosRequestConfig) => {

    // Обновляем токен, если время его жизни истекло
    const currentToken = (config?.headers?.Authorization as string)?.split(" ")[1]
    // console.log('currentToken',currentToken)
    const canTokenRefresh = () => config.url !== `/auth/login`;
    if (!currentToken && canTokenRefresh()) {
      if (!TokenService.isTokenValid(currentToken)) {
        await (storesContext as RootStoreContext)?._currentValue.authStore.refreshToken()
      }
    }
// TODO specify cors before publish
    const headers = ApiHelper.customizeHeaders(config, {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*',
      'Access-Control-Request-Headers':'*',
      ...config.headers,
    });

    const newConfig: AxiosRequestConfig = {
      ...config,
      headers,
    };
    return newConfig;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  },
);

api.interceptors.response.use(
  response => {
    if (!SUCCESS_STATUSES.includes(response.status)) {
      // Notification.showError(JSON.stringify(response.data?.message) || 'Unknown error');

      return Promise.reject(response);
    }

    if (response?.data?.message) {
      // Notification.showSuccess(response?.data?.message);
    }
    return response.data;
  },
  error => {

    // global showing error messages
    console.log('error on url = ', JSON.stringify(error?.response?.config?.url))
    console.log('response error', JSON.stringify(error?.response?.data));

    if (error.response?.status === SERVER_ERROR) {
    } else if (AUTH_ERROR.includes(error.response?.status)) {
      (storesContext as RootStoreContext)?._currentValue.authStore.refreshToken().then(() => {
      })
    }
    else if (BADREQUEST.includes(error?.response?.data?.statusCode)) {
      // DO nothing. Error will be handled in method witch call this error
    } else if (NOT_FOUND_ERROR.includes(error?.response?.data?.statusCode)) {
      // DO nothing. Error will be handled in method witch call this error
    } else {
      // Notification.showError('Произошла ошибка');
    }

    return Promise.reject(error);
  },
);

export const setAccessToken = (token: string) => {
  Object.defineProperty(api.defaults.headers, 'Authorization', {
    enumerable: true,
    configurable: true,
    writable: true,
    value: `Bearer ${token}`
  });

  (storesContext as RootStoreContext)?._currentValue?.authStore?.tokenWasUpdated();
};

export interface IConfig {
  url: string;
  data?: Object;
  config?: AxiosRequestConfig;
}

export interface IResponseCommon<T> {
  success: boolean;
  errors: {
    [key: string]: string[];
  } | null;
  message: string | null;
  data: T | T[];
}
