export default abstract class AbstractLocalRepository {
  abstract tableName(): string;

  get = async (key?: string): Promise<string | undefined> => {
    const data = await localStorage.getItem(key || this.tableName());
    return data ? JSON.parse(data) : null;
  };

  set = async (data: any, key?: string) => {
    return localStorage.setItem(key || this.tableName(), JSON.stringify(data));
  };

  update = async (data: any) => {
    let res = await this.get();

    if (res) {
      for (let k in data) {
        //@ts-ignore
        res[k] = data[k];
      }
    }

    return this.set(res).then();
  };

  removeAll = async () => {
    return localStorage.removeItem(this.tableName());
  };
}
