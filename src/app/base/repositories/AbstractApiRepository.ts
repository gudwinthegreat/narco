import * as qs from 'qs';
import { api, IConfig, IResponseCommon } from '../adapters/ApiAdapter';

export enum ApiOrderType {
  DESC = 'DESC',
  ASC = 'ASC',
}

export interface IApiFindOneOptions<T> {
  select?: (keyof T)[];
  relations?: string[];
  where?: { [P in keyof T]: string }[];
}

export interface IApiFindManyOptions<T> extends IApiFindOneOptions<T> {
  take?: number;
  skip?: number;
  order?: { [P in keyof T]?: ApiOrderType };
}

export default abstract class AbstractApiRepository {
  findAll = <T extends {}>(url: string, options?: IApiFindManyOptions<T>): Promise<[T[], number]> => {
    return api.get(url, {
      params: options,
      paramsSerializer: (params: any) => qs.stringify(params),
    });
  };

  get = <T extends {}>(config: IConfig) => {
    return api.get<IResponseCommon<T>>(config.url);
  };

  post = <T extends {}>(config: IConfig) => {
    return api.post<IResponseCommon<T>>(config.url, config.data);
  };

  put = <T extends {}>(config: IConfig) => {
    return api.put<IResponseCommon<T>>(config.url, config.data);
  };

  patch = <T extends {}>(config: IConfig) => {
    return api.put<IResponseCommon<T>>(config.url, config.data);
  };

  delete = <T extends {}>(config: IConfig) => {
    return api.delete<IResponseCommon<T>>(config.url);
  };
}
