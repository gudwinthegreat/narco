import React from 'react';
import { AuthStore } from '../modules/auth/AuthStore';
import { TabsStore } from '../modules/tabs/TabsStore';

class RootStore {
  authStore: AuthStore;
  tabsStore: TabsStore;

  constructor() {
    this.authStore = new AuthStore();
    this.tabsStore = new TabsStore();
  }

  /**
   * Фнукция проходит по всем сторисам и вызывает функцию sync если она есть.
   * Функция вызывается в файле App.tsx
   */
  sync = async () => {
    await Promise.all(
      Object.values(this).map(store => {
        return store?.sync ? store?.sync() : Promise.resolve();
      }),
    );
  };
}

export const rootStore = new RootStore();

export type RootStoreContext = React.Context<RootStore> & { _currentValue: RootStore }

export const storesContext = React.createContext(rootStore);
