import { observer } from "mobx-react";
import { UserAddOutlined, TableOutlined, SettingOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import { MenuVariants } from "../modules/tabs/models/menuVariants";
import { useRootStore } from "../base/hooks/useRootStore";


export const TopMenu = observer(() => {
    const { tabsStore } = useRootStore();
    const onClick: MenuProps['onClick'] = e => {
        tabsStore.setSelectedMenuItem(e.key)
    };

    const items: MenuProps['items'] = [
        {
            label: 'Принять нового',
            key: MenuVariants.NEW,
            icon: <UserAddOutlined />,
        },
        {
            label: 'Архив',
            key: MenuVariants.ARCHIVE,
            icon: <TableOutlined />
        },
        {
            label: 'Настройки',
            key: MenuVariants.SETTINGS,
            icon: <SettingOutlined />
        }
    ]

    return (
        <>
            <Menu
                onClick={onClick}
                selectedKeys={[tabsStore.selectedMenuItem]}
                mode="horizontal"
                items={items}
            />
        </>
    )
})