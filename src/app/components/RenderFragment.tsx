import { observer } from "mobx-react";
import { useRootStore } from "../base/hooks/useRootStore";
import { MenuVariants } from "../modules/tabs/models/menuVariants"
import { AcceptNewPatient } from "../screens/tabs/acceptNewPatient/AcceptNewPatient";

export const RenderSwitch = observer(() => {
    const { tabsStore } = useRootStore();

    return (() => {
        switch (tabsStore.selectedMenuItem) {
            case MenuVariants.NEW:
                return <AcceptNewPatient />
            case MenuVariants.ARCHIVE:
                return <><p>MenuVariants.ARCHIVE</p></>
            case MenuVariants.SETTINGS:
                return <><p>MenuVariants.SETTINGS</p></>
            default:
                console.error(`RenderSwitch type= ${tabsStore.selectedMenuItem} not implemented`)
                return <><p>Not Implemented</p></>
        }
    })()
})