import TokenLocalRepository from './TokenLocalRepository';

// import {KJUR} from 'jsrsasign'
import { setAccessToken } from '../../base/adapters/ApiAdapter';

export default class TokenService {
  tokenLocal: TokenLocalRepository;

  constructor() {
    this.tokenLocal = new TokenLocalRepository();
  }

  /**
   * Получение токена авторизации из локального хранилища. Сохранение токена в Api Adapter.
   * @returns {string} - токен авторизации.
   */
  public getToken = async () => {
    const token = await this.tokenLocal.get();
    setAccessToken(token as string);
    return token;
  };

  /**
   * Сохранение токена в Api Adapter и в локальном хранилище.
   * @param {string} token - токен авторизации.
   */
  public saveToken = (token: string) => {
    setAccessToken(token);
    return this.tokenLocal.set(token);
  };

  public saveRefreshToken = (token: string) => {
    this.tokenLocal.set(token, "refreshToken")
  }

  public getRefreshToken = () => {
    return this.tokenLocal.get("refreshToken")
  }

  /**
   * Удаление токена из локального хранилища.
   */
  deleteToken = () => {
    return this.tokenLocal.removeAll();
  };

  static isTokenValid = (token: string): boolean => {
    return true
    // if (!token) {
    //   return false
    // }

    // const decodedToken = KJUR.jws.JWS.parse(token)
    // const iat = (decodedToken.payloadObj as any).iat
    // const secondsUTC = ((new Date()).valueOf() / 1000).toFixed(0)
    // const tokensHalfLifeTime = 1800
    // return (iat + tokensHalfLifeTime) > secondsUTC
  }
}
