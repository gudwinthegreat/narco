import AbstractLocalRepository from "../../base/repositories/AbstractLocalRepository";

export default class TokenLocalRepository extends AbstractLocalRepository {
  tableName(): string {
    return 'accessToken';
  }
}
