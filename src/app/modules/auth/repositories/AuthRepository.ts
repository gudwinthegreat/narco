import AbstractApiRepository from "../../../base/repositories/AbstractApiRepository";

export default class AuthApiRepository extends AbstractApiRepository {
    signIn = async (value: any) => {
        console.log("signIn", value)
        return this.post({
            url: '/auth/login',
            data: { login: value.username, password: value.password },
        });
    }

    refreshToken = async (refreshToken: string) => {
        return this.post({
            url: 'auth/refresh',
            data: refreshToken
        })
    }
}