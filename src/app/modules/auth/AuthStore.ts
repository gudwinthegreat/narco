import { makeAutoObservable } from "mobx";
import { Navigate } from "react-router-dom";
import { AuthService } from "./AuthService";

export class AuthStore {

    isLoggedIn: boolean;
    private authService: AuthService;
    constructor() {
        makeAutoObservable(this);

        this.authService = new AuthService();
        this.isLoggedIn = false;
    }

    public signIn = async (value: any) => {
        return this.authService.signIn(value)
    }

    public refreshToken = async () => {
        this.authService.refreshToken()
            .then(() => this.setIsLoggedIn(true))
            .catch(() => {
                this.setIsLoggedIn(false)
                Navigate({ to: "/login", replace: true })
            })
    }

    public tokenWasUpdated = () => {
        this.setIsLoggedIn(true)
    }

    private setIsLoggedIn = (flag: boolean) => {
        this.isLoggedIn = flag
    }

    sync = ()=>{
        // проверить локалстор на наличие токена. 
        // дописать в токенсервайсе проверку на свежесть токена
        // если все ок, то setIsLoggedIn true, иначе обновляем токен, иначе выкидываем пользователя.
    }
}