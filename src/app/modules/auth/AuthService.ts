import { setAccessToken } from "../../base/adapters/ApiAdapter";
import TokenService from "../token/TokenService";
import AuthApiRepository from "./repositories/AuthRepository";

export class AuthService {
    authRepository: AuthApiRepository;
    tokenService: TokenService;
    constructor() {
        this.authRepository = new AuthApiRepository();
        this.tokenService = new TokenService();
    }

    signIn = async (value: any) => {
        const tokens: any = await this.authRepository.signIn(value)
        setAccessToken(tokens.accessToken);
        this.tokenService.saveRefreshToken(tokens.refreshToken)
    }

    refreshToken = async () => {
        const refreshToken = await this.tokenService.getRefreshToken();
        if (refreshToken) {
            const tokens: any = await this.authRepository.refreshToken(refreshToken)
            setAccessToken(tokens.accessToken)
            this.tokenService.saveRefreshToken(tokens.refreshToken)
        } 
    }

}