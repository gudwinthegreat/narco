import { makeAutoObservable } from "mobx";
import { PatientService } from "./PatientService";

export class PatientStore {

    private patientService: PatientService;
    constructor() {
        makeAutoObservable(this)

        this.patientService = new PatientService()
    }

}