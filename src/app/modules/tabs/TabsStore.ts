import { makeAutoObservable } from "mobx";
import { MenuVariants } from "./models/menuVariants";

export class TabsStore {
    public selectedMenuItem: MenuVariants | string;

    constructor() {
        makeAutoObservable(this);

        this.selectedMenuItem = MenuVariants.NEW
    }

    public setSelectedMenuItem = (variant: MenuVariants | string) => {
        this.selectedMenuItem = variant
    }
}


