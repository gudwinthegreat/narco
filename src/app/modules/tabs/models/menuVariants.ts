export enum MenuVariants {
    NEW = "NEW",
    ARCHIVE = "ARCHIVE",
    SETTINGS = "SETTINGS"
}