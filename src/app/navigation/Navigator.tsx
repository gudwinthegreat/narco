import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import { Login } from '../screens/authScreen/Login';
import { Form307 } from "../screens/Form307/Form307";
import { Greetings } from "../screens/greetingScreen/Greetings";
import { MainScreen } from "../screens/mainScreen/MainScreen";
import Protected from "../screens/Protected";
import { TestScreen } from "../screens/testScreen/TestScreen";



export const Navigator = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Greetings />} />
                <Route path="/login" element={<Login />} />
                <Route path='/mainScreen' element={<MainScreen />} />
                {/* TODO change to protected route */}
                <Route path='/mainScreen' element={
                    <Protected
                        children={<MainScreen />}
                    />}
                />
                <Route path='/form307' element={<Form307 />} />
                <Route path='/test' element={<TestScreen />} />
            </Routes>
        </BrowserRouter>
    )

}