/**
 * Конфигурационный файл со следующими параметрами:
 * 1. apiUrl - ссылка на апи сервер.
 * 2. phone - номер телефона.
 * 3. ambulancePhone - номер телефона скорой помощи.
 * 3. appVersion - версия приложения. Например: 1.1.1(1)
 * 4. deviceId - уникальный идентификатор устройства пользователя.
 * 5. customHeaders - кастомные настройки для mock api header.
 */
export const appConfig = {
  apiUrl: 'http://localhost:3000',
  phone: '8 800 500 51 49',
  ambulancePhone: '112',
  customHeaders: null
};
