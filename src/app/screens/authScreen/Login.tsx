import { Button, Col, Form, Input, Row } from 'antd';
import './Login.css'
import { observer } from 'mobx-react';
import { useRootStore } from '../../base/hooks/useRootStore';
import { useState } from 'react';
import { useNavigate } from "react-router-dom";

export const Login = observer(() => {
    const { authStore } = useRootStore();
    const [logOrPassInvalid, setLogOrPassInvalid] = useState<boolean>(false)
    const navigate = useNavigate();

    const onFinish = (values: any) => {
        authStore.signIn(values).then(() => {
            setLogOrPassInvalid(false)
            navigate('/mainScreen')
        }).catch(() => {
            setLogOrPassInvalid(true)
        })
    };

    return (
        <>
            <Row>
                <Col span={8} />
                <Col span={8}>
                    <Form
                        className='auth-form'
                        name="basic"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        initialValues={{ remember: true }}
                        onFinish={onFinish}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Username"
                            name="username"
                            rules={[
                                { required: true, message: 'Please input your username!' },
                            ]}
                            help={logOrPassInvalid ? "username or password incorrect" : ''}
                            validateStatus={logOrPassInvalid ? 'error' : 'success'}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[
                                { required: true, message: 'Please input your password!' },
                            ]}
                            help={logOrPassInvalid ? "username or password incorrect" : ''}
                            validateStatus={logOrPassInvalid ? 'error' : 'success'}
                        >
                            <Input.Password />
                        </Form.Item>

                        {/* <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                            <Checkbox>Remember me</Checkbox>
                        </Form.Item> */}

                        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
                <Col span={8} />
            </Row>
        </>
    )
})