import { observer } from "mobx-react";
import { TopMenu } from "../../components/Menu";
import { RenderSwitch } from "../../components/RenderFragment";

export const MainScreen = observer(() => {

    return <>
        <TopMenu />
        <RenderSwitch />
    </>
})