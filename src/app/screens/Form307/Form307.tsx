import { Card, Col, Row } from "antd"
import { observer } from "mobx-react"
import { Space, Typography } from 'antd';
import { useRef } from "react";
import ReactToPrint from 'react-to-print';

const { Text, Title, Paragraph } = Typography;

export const Form307 = observer(() => {

    const componentRef = useRef(null);

    const actNumber = 4
    const date = new Date()
    return (
        <>
            <ReactToPrint
                content={() => componentRef.current}
            />

            <Card ref={componentRef}  >
                <Row>
                    <Col span={11} push={1}>
                        <Space direction="vertical">
                            <Text>______________________________________</Text>
                            <Text>______________________________________</Text>
                            <Text>______________________________________</Text>
                            {/* <Text type="secondary">(наименование медицинской организации</Text>
                            <Text type="secondary">адрес местонахождения, номер и дата</Text>
                            <Text type="secondary">получения лицензии на право проведения</Text>
                            <Text type="secondary">медицинского освидетельствования на</Text>
                            <Text type="secondary">состояние опьянения (алкогольного,</Text>
                            <Text type="secondary">наркотического или иного токсического)</Text> */}
                        </Space>
                    </Col>
                    <Col span={11} push={1}>
                        <Space direction="vertical">
                            <Text> Медицинская документация</Text>
                            <Text>Учетная форма N 307/у-05</Text>
                            <Text>Утверждена приказом Министерства</Text>
                            <Text>здравоохранения Российской Федерации</Text>
                            <Text>от 18 декабря 2015 г. N 933н</Text>
                        </Space>
                    </Col>
                </Row>
                <Row>
                    <Col span={12} offset={6}>
                        <Title
                            level={5}
                            style={{ flex: 1, textAlign: 'center', paddingTop: 16 }}
                            children={[
                                <p style={{ marginBottom: 0 }}>Акт</p>,
                                <p style={{ marginBottom: 0 }}>медицинского освидетельствования на состояние опьянения</p>,
                                <p style={{ marginBottom: 0 }}>(алкогольного, наркотического или иного токсического)</p>,
                                <p style={{ marginBottom: 0 }}>{`N ${actNumber}`}</p>
                            ]} />
                    </Col>
                </Row>
                <Row>
                    <Col span={18} push={6}>
                        <Text>{`${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`}</Text>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <Space direction="vertical">
                            <Paragraph>1. Сведения об освидетельствуемом лице:</Paragraph>
                            <Paragraph>Фамилия, имя, отчество (при наличии)</Paragraph>
                            <Paragraph>Дата рождения</Paragraph>
                            <Paragraph>Адрес места жительства</Paragraph>
                            <Paragraph>Сведения об освидетельствуемом лице заполнены на основании</Paragraph>
                            <Paragraph children={[
                                <p style={{ marginBottom: 0 }}>2. Основание для медицинского освидетельствования</p>,
                                <Text type="secondary">(протокол  о  направлении  на  медицинское  освидетельствование, письменное
                                    направление  работодателя,  личное  заявление,  фамилия, имя, отчество (при
                                    наличии)     должностного     лица,     направившего     на     медицинское
                                    освидетельствование)
                                </Text>
                            ]} />
                            <Paragraph>3. Наименование  структурного  подразделения  медицинской  организации,  в
                                котором проводится медицинское освидетельствование
                            </Paragraph>
                            <Paragraph>4. Дата и точное время начала медицинского освидетельствования</Paragraph>
                            <Paragraph children={[
                                <p style={{ marginBottom: 0 }}>
                                    5.  Кем освидетельствован
                                </p>,
                                <Text type='secondary'>(должность, фамилия и инициалы врача (фельдшера),
                                    сведения  о  прохождении  подготовки  по  вопросам  проведения медицинского
                                    освидетельствования:   наименование  медицинской  организации, дата  выдачи
                                    документа)
                                </Text>
                            ]} />
                            <Paragraph> 6.  Внешний вид освидетельствуемого (наличие видимых повреждений, следов от
                                инъекций)
                            </Paragraph>
                            <Paragraph>7. Жалобы освидетельствуемого на свое состояние</Paragraph>
                            <Paragraph>8. Изменения психической деятельности освидетельствуемого</Paragraph>
                            <Text>результат пробы Шульте</Text>
                            <Paragraph>9. Вегетативно-сосудистые реакции освидетельствуемого</Paragraph>
                            <Space direction="horizontal">
                                <Text>зрачки</Text>
                                <Text type="secondary">(сужены, расширены, в норме)</Text>
                            </Space>
                            <Space direction="horizontal">
                                <Text>реакция на свет</Text>
                                <Text type="secondary">(живая, вялая)</Text>
                            </Space>
                            <Text>склеры</Text>
                            <Text>нистагм</Text>
                            <Paragraph>10. Двигательная сфера освидетельствуемого</Paragraph>
                            <Text>речь</Text>
                            <Text>походка</Text>
                            <Space direction="horizontal">
                                <Text>устойчивость в позе Ромберга</Text>
                                <Text type="secondary">(устойчив, неустойчив)</Text>
                            </Space>
                            <Text>точность выполнения координационных проб</Text>
                            <Text>результат пробы Ташена</Text>
                            <Space direction="horizontal">
                                <Text>11.   Наличие   заболеваний   нервной   системы,  психических  расстройств,
                                    перенесенных травм </Text>
                                <Text type="secondary">(со слов освидетельствуемого)</Text>
                            </Space>
                            <Space direction="horizontal">
                                <Text>12.  Сведения  о  последнем  употреблении  алкоголя, лекарственных средств,
                                    наркотических средств и психотропных веществ </Text>
                                <Text type="secondary">(со слов освидетельствуемого)</Text>
                            </Space>
                            <Paragraph children={[
                                <p style={{ marginBottom: 0 }}>13. Наличие алкоголя в выдыхаемом воздухе освидетельствуемого</p>,
                                <p style={{ marginBottom: 0 }}>13.1.   Время  первого  исследования,  наименование  технического  средства
                                    измерения,   его  заводской  номер,  дата  последней  поверки,  погрешность
                                    технического средства измерения, результат исследования</p>
                            ]} />
                            <Paragraph children={[
                                <p style={{ marginBottom: 0 }}>Второе  исследование  через  15  -  20  минут:  время  исследования,
                                    результат  исследования
                                </p>,
                                <Text type="secondary">(наименование технического средства измерения, его
                                    заводской  номер, дата последней поверки, погрешность технического средства
                                    измерения  указываются в случае использования другого технического средства
                                    измерения)</Text>
                            ]} />
                            <Paragraph>14. Время отбора биологического объекта у освидетельствуемого</Paragraph>
                            <Text children={[
                                <p>Результаты  химико-токсикологических  исследований  биологических  объектов</p>,
                                <Text type="secondary">(название  лаборатории, методы исследований, результаты исследований, номер
                                    справки о результатах химико-токсикологических исследований)</Text>
                            ]} />
                            <Paragraph children={[
                                <Text>15.  Другие  данные  медицинского  осмотра  или  представленных  документов</Text>,
                                <Text type="secondary">(указать, какие, дату проведенных медицинских вмешательств)</Text>
                            ]} />
                            <Paragraph>16. Дата и точное время окончания медицинского освидетельствования</Paragraph>
                            <Paragraph>17. Медицинское заключение, дата его вынесения</Paragraph>
                            <Paragraph>18. Подпись врача (фельдшера)</Paragraph>
                        </Space>
                    </Col>
                </Row>
            </Card>

        </>
    )
})