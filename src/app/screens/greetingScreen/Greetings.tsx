import { Button, PageHeader } from 'antd';
import './Greetings.css'
import { useNavigate } from "react-router-dom";

export const Greetings = () => {
    const navigate = useNavigate();

    return (
        <PageHeader
            className="site-page-header"
            // onBack={() => null}
            // title="Title"
            // subTitle="This is a subtitle"
            extra={[
                <Button 
                key='1'
                onClick={()=>{
                    navigate('/login')
                }} > Login</Button>
            ]}
        />
    )
}