import { observer } from "mobx-react";
import { Button, message, Steps } from 'antd';
import { useState } from "react";
import { PersonalData } from "./steps/personalData/PersonalData";


export const AcceptNewPatient = observer(() => {
    const [current, setCurrent] = useState(0);
    const next = () => {
        setCurrent(current + 1);
    };

    const prev = () => {
        setCurrent(current - 1);
    };

    const steps = [
        {
            title: 'Персональные данные',
            content: <PersonalData/>,
        },
        {
            title: 'Осмотр',
            content: 'Осмотр',
        },
        {
            title: 'Инструментальные исследования',
            content: 'Инструментальные исследования',
        },
    ];

    const items = steps.map(item => ({ key: item.title, title: item.title }));
    return (
        <div style={{paddingTop:'1em'}}>
            {/* @ts-ignore */}
            <Steps current={current} items={items} />
            <div className="steps-content">{steps[current].content}</div>
            <div className="steps-action">
                {current < steps.length - 1 && (
                    <Button type="primary" onClick={() => next()}>
                        Далее
                    </Button>
                )}
                {current === steps.length - 1 && (
                    <Button type="primary" onClick={() => message.success('Даные отправлены!')}>
                        Закончить
                    </Button>
                )}
                {current > 0 && (
                    <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
                        Назад
                    </Button>
                )}
            </div>
        </div>
    )
})