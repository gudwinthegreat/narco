import { Col, Input, Row } from "antd";
import { observer } from "mobx-react";
import moment from "moment";

export const PersonalData = observer(() => {
    return (
        <>
            <Row gutter={[16, 24]}>
                <Col className="gutter-row" span={24}>
                    <Input
                        addonBefore={'Дата начала экспертизы'}
                        placeholder="..."
                        onChange={(e) => { console.log(e) }}
                        type='datetime-local'
                        defaultValue={moment().format('YYYY-MM-DD[T]HH:mm:ss')}
                    />
                    <Input
                        addonBefore={'ФИО'}
                        placeholder="..."
                        onChange={(e) => { console.log(e) }}
                    />
                    <Input
                        addonBefore={'Дата рождения'}
                        placeholder="..."
                        onChange={(e) => { console.log(e) }}
                        type='date'
                    />
                    <Input
                        addonBefore={'Адрес'}
                        placeholder="..."
                        onChange={(e) => { console.log(e) }}
                    />
                    <Input
                        addonBefore={'Сведения об освидетельствуемом лице заполнены на основании'}
                        placeholder="..."
                        onChange={(e) => { console.log(e) }}
                    />
                    <Input
                        addonBefore={'Основание для медицинского освидетельствования'}
                        placeholder="..."
                        onChange={(e) => { console.log(e) }}
                    />
                </Col>
                <Col className="gutter-row" span={12}></Col>
            </Row>
        </>
    )
})