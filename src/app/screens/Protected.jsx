import { useNavigate } from "react-router-dom";
import { useRootStore } from '../base/hooks/useRootStore'

const Protected = ({ children }) => {
    const { authStore } = useRootStore();
    const replace = useNavigate();
    if (!authStore.isLoggedIn) {
        return replace('/login')
    }
    return children;
};
export default Protected;