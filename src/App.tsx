import './App.css';

import { Navigator } from './app/navigation/Navigator';

export default function App() {
  return (
    <div className="App">
      <Navigator />
    </div>
  );
}
